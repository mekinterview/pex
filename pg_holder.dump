--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE pex_readonly;
ALTER ROLE pex_readonly WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'SCRAM-SHA-256$4096:vMlgX73aoo/S4LvNCqqPfA==$10g5FG9Bb8ccf+P9qHeNQwLf6bmiXATjhJr99NRUa4M=:KXjk5DuoFBVwh6QPE88mCOMcRdBWm9LzLn9tcjblLBw=';
CREATE ROLE pex_superuser;
ALTER ROLE pex_superuser WITH SUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'SCRAM-SHA-256$4096:PA/jjT0K622N0IjNoTB73g==$GWC63fWENTxNlnvznu4kgZR1FfEi6n4LtQb376KHiTk=:gSGCDV0zFr8qs9cuIqVsOCMZzgR8VVG0BMztn9pw46c=';
CREATE ROLE pex_write;
ALTER ROLE pex_write WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'SCRAM-SHA-256$4096:ljGNwSMc3SFuz7dCSFONWw==$2rdqQwv2NDf+Luw/zBtDgkH12c2/1gj7hg3i85ohm3s=:gqKPnuA3kQQtZkPdvkFNX0Y/UmCu9Bj/2lc92VCkpiE=';
CREATE ROLE postgres;
ALTER ROLE postgres WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'SCRAM-SHA-256$4096:a7BvES8MbcPGnX5VAtZ6Qg==$Q73fmN1nqpvklEerd7+9QDVKYmGYiVI+xd4QhEYh4GA=:Js258EVVh1RKoSis8TSyjFh/XShtyMI7p6pPOjYOS/o=';






--
-- Databases
--

--
-- Database "template1" dump
--

\connect template1

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- PostgreSQL database dump complete
--

--
-- Database "pex" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: pex; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE pex WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE pex OWNER TO postgres;

\connect pex

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: music; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA music;


ALTER SCHEMA music OWNER TO postgres;

--
-- Name: track_changes(); Type: FUNCTION; Schema: music; Owner: postgres
--

CREATE FUNCTION music.track_changes() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
INSERT INTO music.track_changes (old_album_id, new_album_id)
VALUES (OLD.album_id, NEW.album_id);
RETURN NEW;
END;$$;


ALTER FUNCTION music.track_changes() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: album; Type: TABLE; Schema: music; Owner: postgres
--

CREATE TABLE music.album (
    album_id integer NOT NULL,
    title character varying(160) NOT NULL
);


ALTER TABLE music.album OWNER TO postgres;

--
-- Name: album_album_id_seq; Type: SEQUENCE; Schema: music; Owner: postgres
--

CREATE SEQUENCE music.album_album_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE music.album_album_id_seq OWNER TO postgres;

--
-- Name: album_album_id_seq; Type: SEQUENCE OWNED BY; Schema: music; Owner: postgres
--

ALTER SEQUENCE music.album_album_id_seq OWNED BY music.album.album_id;


--
-- Name: artist; Type: TABLE; Schema: music; Owner: postgres
--

CREATE TABLE music.artist (
    artist_id integer NOT NULL,
    name character varying(120) NOT NULL
);


ALTER TABLE music.artist OWNER TO postgres;

--
-- Name: artist_artist_id_seq; Type: SEQUENCE; Schema: music; Owner: postgres
--

CREATE SEQUENCE music.artist_artist_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE music.artist_artist_id_seq OWNER TO postgres;

--
-- Name: artist_artist_id_seq; Type: SEQUENCE OWNED BY; Schema: music; Owner: postgres
--

ALTER SEQUENCE music.artist_artist_id_seq OWNED BY music.artist.artist_id;


--
-- Name: artist_right_holder; Type: TABLE; Schema: music; Owner: postgres
--

CREATE TABLE music.artist_right_holder (
    id bigint NOT NULL,
    artist_id integer NOT NULL,
    album_id integer NOT NULL
);


ALTER TABLE music.artist_right_holder OWNER TO postgres;

--
-- Name: artist_right_holder_id_seq; Type: SEQUENCE; Schema: music; Owner: postgres
--

CREATE SEQUENCE music.artist_right_holder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE music.artist_right_holder_id_seq OWNER TO postgres;

--
-- Name: artist_right_holder_id_seq; Type: SEQUENCE OWNED BY; Schema: music; Owner: postgres
--

ALTER SEQUENCE music.artist_right_holder_id_seq OWNED BY music.artist_right_holder.id;


--
-- Name: org; Type: TABLE; Schema: music; Owner: postgres
--

CREATE TABLE music.org (
    org_id integer NOT NULL,
    name character varying(120) NOT NULL
);


ALTER TABLE music.org OWNER TO postgres;

--
-- Name: org_org_id_seq; Type: SEQUENCE; Schema: music; Owner: postgres
--

CREATE SEQUENCE music.org_org_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE music.org_org_id_seq OWNER TO postgres;

--
-- Name: org_org_id_seq; Type: SEQUENCE OWNED BY; Schema: music; Owner: postgres
--

ALTER SEQUENCE music.org_org_id_seq OWNED BY music.org.org_id;


--
-- Name: org_right_holder; Type: TABLE; Schema: music; Owner: postgres
--

CREATE TABLE music.org_right_holder (
    id integer NOT NULL,
    org_id integer NOT NULL,
    album_id integer NOT NULL
);


ALTER TABLE music.org_right_holder OWNER TO postgres;

--
-- Name: org_right_holder_id_seq; Type: SEQUENCE; Schema: music; Owner: postgres
--

CREATE SEQUENCE music.org_right_holder_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE music.org_right_holder_id_seq OWNER TO postgres;

--
-- Name: org_right_holder_id_seq; Type: SEQUENCE OWNED BY; Schema: music; Owner: postgres
--

ALTER SEQUENCE music.org_right_holder_id_seq OWNED BY music.org_right_holder.id;


--
-- Name: track; Type: TABLE; Schema: music; Owner: postgres
--

CREATE TABLE music.track (
    track_id integer NOT NULL,
    name character varying(200) NOT NULL,
    album_id integer,
    milliseconds integer NOT NULL,
    bytes integer DEFAULT 0 NOT NULL
);


ALTER TABLE music.track OWNER TO postgres;

--
-- Name: track_changes; Type: TABLE; Schema: music; Owner: postgres
--

CREATE TABLE music.track_changes (
    track_changes_id bigint NOT NULL,
    old_album_id integer NOT NULL,
    new_album_id integer NOT NULL
);


ALTER TABLE music.track_changes OWNER TO postgres;

--
-- Name: track_changes_track_changes_id_seq; Type: SEQUENCE; Schema: music; Owner: postgres
--

CREATE SEQUENCE music.track_changes_track_changes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE music.track_changes_track_changes_id_seq OWNER TO postgres;

--
-- Name: track_changes_track_changes_id_seq; Type: SEQUENCE OWNED BY; Schema: music; Owner: postgres
--

ALTER SEQUENCE music.track_changes_track_changes_id_seq OWNED BY music.track_changes.track_changes_id;


--
-- Name: track_track_id_seq; Type: SEQUENCE; Schema: music; Owner: postgres
--

CREATE SEQUENCE music.track_track_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE music.track_track_id_seq OWNER TO postgres;

--
-- Name: track_track_id_seq; Type: SEQUENCE OWNED BY; Schema: music; Owner: postgres
--

ALTER SEQUENCE music.track_track_id_seq OWNED BY music.track.track_id;


--
-- Name: users; Type: TABLE; Schema: music; Owner: postgres
--

CREATE TABLE music.users (
    user_id integer NOT NULL,
    org_id integer NOT NULL,
    username character varying(50) NOT NULL,
    password character varying(32) NOT NULL
);


ALTER TABLE music.users OWNER TO postgres;

--
-- Name: users_user_id_seq; Type: SEQUENCE; Schema: music; Owner: postgres
--

CREATE SEQUENCE music.users_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE music.users_user_id_seq OWNER TO postgres;

--
-- Name: users_user_id_seq; Type: SEQUENCE OWNED BY; Schema: music; Owner: postgres
--

ALTER SEQUENCE music.users_user_id_seq OWNED BY music.users.user_id;


--
-- Name: album album_id; Type: DEFAULT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.album ALTER COLUMN album_id SET DEFAULT nextval('music.album_album_id_seq'::regclass);


--
-- Name: artist artist_id; Type: DEFAULT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.artist ALTER COLUMN artist_id SET DEFAULT nextval('music.artist_artist_id_seq'::regclass);


--
-- Name: artist_right_holder id; Type: DEFAULT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.artist_right_holder ALTER COLUMN id SET DEFAULT nextval('music.artist_right_holder_id_seq'::regclass);


--
-- Name: org org_id; Type: DEFAULT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.org ALTER COLUMN org_id SET DEFAULT nextval('music.org_org_id_seq'::regclass);


--
-- Name: org_right_holder id; Type: DEFAULT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.org_right_holder ALTER COLUMN id SET DEFAULT nextval('music.org_right_holder_id_seq'::regclass);


--
-- Name: track track_id; Type: DEFAULT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.track ALTER COLUMN track_id SET DEFAULT nextval('music.track_track_id_seq'::regclass);


--
-- Name: track_changes track_changes_id; Type: DEFAULT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.track_changes ALTER COLUMN track_changes_id SET DEFAULT nextval('music.track_changes_track_changes_id_seq'::regclass);


--
-- Name: users user_id; Type: DEFAULT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.users ALTER COLUMN user_id SET DEFAULT nextval('music.users_user_id_seq'::regclass);


--
-- Data for Name: album; Type: TABLE DATA; Schema: music; Owner: postgres
--

COPY music.album (album_id, title) FROM stdin;
\.


--
-- Data for Name: artist; Type: TABLE DATA; Schema: music; Owner: postgres
--

COPY music.artist (artist_id, name) FROM stdin;
1	MEK
\.


--
-- Data for Name: artist_right_holder; Type: TABLE DATA; Schema: music; Owner: postgres
--

COPY music.artist_right_holder (id, artist_id, album_id) FROM stdin;
\.


--
-- Data for Name: org; Type: TABLE DATA; Schema: music; Owner: postgres
--

COPY music.org (org_id, name) FROM stdin;
\.


--
-- Data for Name: org_right_holder; Type: TABLE DATA; Schema: music; Owner: postgres
--

COPY music.org_right_holder (id, org_id, album_id) FROM stdin;
\.


--
-- Data for Name: track; Type: TABLE DATA; Schema: music; Owner: postgres
--

COPY music.track (track_id, name, album_id, milliseconds, bytes) FROM stdin;
\.


--
-- Data for Name: track_changes; Type: TABLE DATA; Schema: music; Owner: postgres
--

COPY music.track_changes (track_changes_id, old_album_id, new_album_id) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: music; Owner: postgres
--

COPY music.users (user_id, org_id, username, password) FROM stdin;
\.


--
-- Name: album_album_id_seq; Type: SEQUENCE SET; Schema: music; Owner: postgres
--

SELECT pg_catalog.setval('music.album_album_id_seq', 2, true);


--
-- Name: artist_artist_id_seq; Type: SEQUENCE SET; Schema: music; Owner: postgres
--

SELECT pg_catalog.setval('music.artist_artist_id_seq', 1, true);


--
-- Name: artist_right_holder_id_seq; Type: SEQUENCE SET; Schema: music; Owner: postgres
--

SELECT pg_catalog.setval('music.artist_right_holder_id_seq', 1, false);


--
-- Name: org_org_id_seq; Type: SEQUENCE SET; Schema: music; Owner: postgres
--

SELECT pg_catalog.setval('music.org_org_id_seq', 1, false);


--
-- Name: org_right_holder_id_seq; Type: SEQUENCE SET; Schema: music; Owner: postgres
--

SELECT pg_catalog.setval('music.org_right_holder_id_seq', 1, false);


--
-- Name: track_changes_track_changes_id_seq; Type: SEQUENCE SET; Schema: music; Owner: postgres
--

SELECT pg_catalog.setval('music.track_changes_track_changes_id_seq', 2, true);


--
-- Name: track_track_id_seq; Type: SEQUENCE SET; Schema: music; Owner: postgres
--

SELECT pg_catalog.setval('music.track_track_id_seq', 1, true);


--
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: music; Owner: postgres
--

SELECT pg_catalog.setval('music.users_user_id_seq', 1, false);


--
-- Name: artist_right_holder artist_right_holder_pkey; Type: CONSTRAINT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.artist_right_holder
    ADD CONSTRAINT artist_right_holder_pkey PRIMARY KEY (id, artist_id, album_id);


--
-- Name: album pk_album; Type: CONSTRAINT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.album
    ADD CONSTRAINT pk_album PRIMARY KEY (album_id);


--
-- Name: artist pk_artist; Type: CONSTRAINT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.artist
    ADD CONSTRAINT pk_artist PRIMARY KEY (artist_id);


--
-- Name: org pk_org; Type: CONSTRAINT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.org
    ADD CONSTRAINT pk_org PRIMARY KEY (org_id);


--
-- Name: org_right_holder pk_org_right_holder; Type: CONSTRAINT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.org_right_holder
    ADD CONSTRAINT pk_org_right_holder PRIMARY KEY (id);


--
-- Name: track pk_track; Type: CONSTRAINT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.track
    ADD CONSTRAINT pk_track PRIMARY KEY (track_id);


--
-- Name: track_changes pk_track_changes; Type: CONSTRAINT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.track_changes
    ADD CONSTRAINT pk_track_changes PRIMARY KEY (track_changes_id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);


--
-- Name: track track_changes; Type: TRIGGER; Schema: music; Owner: postgres
--

CREATE TRIGGER track_changes BEFORE UPDATE OF album_id ON music.track FOR EACH ROW EXECUTE FUNCTION music.track_changes();


--
-- Name: artist_right_holder artist_right_holder_album_id_fkey; Type: FK CONSTRAINT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.artist_right_holder
    ADD CONSTRAINT artist_right_holder_album_id_fkey FOREIGN KEY (album_id) REFERENCES music.album(album_id);


--
-- Name: artist_right_holder artist_right_holder_artist_id_fkey; Type: FK CONSTRAINT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.artist_right_holder
    ADD CONSTRAINT artist_right_holder_artist_id_fkey FOREIGN KEY (artist_id) REFERENCES music.artist(artist_id);


--
-- Name: track fk_album_id; Type: FK CONSTRAINT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.track
    ADD CONSTRAINT fk_album_id FOREIGN KEY (album_id) REFERENCES music.album(album_id) NOT VALID;


--
-- Name: org_right_holder fk_org_id; Type: FK CONSTRAINT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.org_right_holder
    ADD CONSTRAINT fk_org_id FOREIGN KEY (org_id) REFERENCES music.org(org_id) NOT VALID;


--
-- Name: org_right_holder org_right_holder_org_id_fkey; Type: FK CONSTRAINT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.org_right_holder
    ADD CONSTRAINT org_right_holder_org_id_fkey FOREIGN KEY (org_id) REFERENCES music.org(org_id) NOT VALID;


--
-- Name: users users_org_id_fkey; Type: FK CONSTRAINT; Schema: music; Owner: postgres
--

ALTER TABLE ONLY music.users
    ADD CONSTRAINT users_org_id_fkey FOREIGN KEY (org_id) REFERENCES music.org(org_id);


--
-- Name: DATABASE pex; Type: ACL; Schema: -; Owner: postgres
--

GRANT CONNECT ON DATABASE pex TO pex_readonly;
GRANT CONNECT ON DATABASE pex TO pex_write;
GRANT CONNECT ON DATABASE pex TO pex_superuser;


--
-- Name: SCHEMA music; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA music TO pex_readonly;
GRANT USAGE ON SCHEMA music TO pex_write;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;


--
-- Name: TABLE album; Type: ACL; Schema: music; Owner: postgres
--

GRANT SELECT ON TABLE music.album TO pex_readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE music.album TO pex_write;


--
-- Name: SEQUENCE album_album_id_seq; Type: ACL; Schema: music; Owner: postgres
--

GRANT SELECT ON SEQUENCE music.album_album_id_seq TO pex_readonly;
GRANT ALL ON SEQUENCE music.album_album_id_seq TO pex_write;


--
-- Name: TABLE artist; Type: ACL; Schema: music; Owner: postgres
--

GRANT SELECT ON TABLE music.artist TO pex_readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE music.artist TO pex_write;


--
-- Name: SEQUENCE artist_artist_id_seq; Type: ACL; Schema: music; Owner: postgres
--

GRANT SELECT ON SEQUENCE music.artist_artist_id_seq TO pex_readonly;
GRANT ALL ON SEQUENCE music.artist_artist_id_seq TO pex_write;


--
-- Name: TABLE artist_right_holder; Type: ACL; Schema: music; Owner: postgres
--

GRANT SELECT ON TABLE music.artist_right_holder TO pex_readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE music.artist_right_holder TO pex_write;


--
-- Name: SEQUENCE artist_right_holder_id_seq; Type: ACL; Schema: music; Owner: postgres
--

GRANT SELECT ON SEQUENCE music.artist_right_holder_id_seq TO pex_readonly;
GRANT ALL ON SEQUENCE music.artist_right_holder_id_seq TO pex_write;


--
-- Name: TABLE org; Type: ACL; Schema: music; Owner: postgres
--

GRANT SELECT ON TABLE music.org TO pex_readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE music.org TO pex_write;


--
-- Name: SEQUENCE org_org_id_seq; Type: ACL; Schema: music; Owner: postgres
--

GRANT SELECT ON SEQUENCE music.org_org_id_seq TO pex_readonly;
GRANT ALL ON SEQUENCE music.org_org_id_seq TO pex_write;


--
-- Name: TABLE org_right_holder; Type: ACL; Schema: music; Owner: postgres
--

GRANT SELECT ON TABLE music.org_right_holder TO pex_readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE music.org_right_holder TO pex_write;


--
-- Name: SEQUENCE org_right_holder_id_seq; Type: ACL; Schema: music; Owner: postgres
--

GRANT SELECT ON SEQUENCE music.org_right_holder_id_seq TO pex_readonly;
GRANT ALL ON SEQUENCE music.org_right_holder_id_seq TO pex_write;


--
-- Name: TABLE track; Type: ACL; Schema: music; Owner: postgres
--

GRANT SELECT ON TABLE music.track TO pex_readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE music.track TO pex_write;


--
-- Name: TABLE track_changes; Type: ACL; Schema: music; Owner: postgres
--

GRANT SELECT ON TABLE music.track_changes TO pex_readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE music.track_changes TO pex_write;


--
-- Name: SEQUENCE track_changes_track_changes_id_seq; Type: ACL; Schema: music; Owner: postgres
--

GRANT SELECT ON SEQUENCE music.track_changes_track_changes_id_seq TO pex_readonly;
GRANT ALL ON SEQUENCE music.track_changes_track_changes_id_seq TO pex_write;


--
-- Name: SEQUENCE track_track_id_seq; Type: ACL; Schema: music; Owner: postgres
--

GRANT SELECT ON SEQUENCE music.track_track_id_seq TO pex_readonly;
GRANT ALL ON SEQUENCE music.track_track_id_seq TO pex_write;


--
-- Name: TABLE users; Type: ACL; Schema: music; Owner: postgres
--

GRANT SELECT ON TABLE music.users TO pex_readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE music.users TO pex_write;


--
-- Name: SEQUENCE users_user_id_seq; Type: ACL; Schema: music; Owner: postgres
--

GRANT SELECT ON SEQUENCE music.users_user_id_seq TO pex_readonly;
GRANT ALL ON SEQUENCE music.users_user_id_seq TO pex_write;


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: music; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE pex_superuser IN SCHEMA music GRANT SELECT ON SEQUENCES  TO pex_readonly;
ALTER DEFAULT PRIVILEGES FOR ROLE pex_superuser IN SCHEMA music GRANT ALL ON SEQUENCES  TO pex_write;


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: music; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA music GRANT SELECT ON SEQUENCES  TO pex_readonly;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA music GRANT ALL ON SEQUENCES  TO pex_write;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: music; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE pex_superuser IN SCHEMA music GRANT SELECT ON TABLES  TO pex_readonly;
ALTER DEFAULT PRIVILEGES FOR ROLE pex_superuser IN SCHEMA music GRANT SELECT,INSERT,DELETE,UPDATE ON TABLES  TO pex_write;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: music; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA music GRANT SELECT ON TABLES  TO pex_readonly;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA music GRANT SELECT,INSERT,DELETE,UPDATE ON TABLES  TO pex_write;


--
-- PostgreSQL database dump complete
--

--
-- Database "postgres" dump
--

\connect postgres

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--
