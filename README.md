1.	Create a SQL Database

Create a new Postgres database server that consists of the following:

```
●	A new schema which allows us to model:
    ○	Users
        ■	With administrative privileges
        ■	With write privileges
        ■	With read-only privileges
    ○	Music Albums & Tracks
    ○	Rights-Holder(owners of an album or track) with catalogs of albums and or tracks
    ○	Rights-Holder Organization and Users Authentication
    ➕	BONUS (not required)
        ■	Model Compilation Albums OR
        ■	Add the ability to track changes in ownership of albums and tracks for compilations
++++++ I added trigger for the track this changes ++++++

Create a database dump containing the schema and anything else needed for a reviewer to reconstitute your database.

●	Post your submission to a publicly accessible Github or other SCCS and send us a link.
💡	Hint: Don’t assume the reviewer has the same environment as you do

```

[+ You can find the dump(pg_holder.dump) file in this repostory. +]

[+ user: pex_readonly password: pex_readonly +]

[+ user: pex_superuser password: pex_superuser +]

[+ user: pex_write password: pex_write +]



2.	Scripting / Automating

Using a tool or language of your choice, create a script or otherwise automate the following tasks. However you choose to achieve this challenge, you must be able to produce one or more artifacts to submit for evaluation. As with above, don’t assume the reviewer has the same environment or prior knowledge as you do.

```
●	Simultaneously open SSH connections to 20 VM’s with the following IP range 192.168.100-120. We understand that you may not have access to 20 hosts to test this against. Just confirm that it is able to connect and do work on more than one host at a time, and that the host range can be specified either as a variable or hard coded.
    ○	Upgrade Postgres Server (Debian)
        →	There can be many steps in this process. Be sure to consider all of them and their order
        →	Confirm the upgrade is successful and Postgres is accepting connections
➕	BONUS (not required): Find all long running (> 1 mins) queries for user “web”, and kill them.

```

```bash
psql -c "
        SELECT
        pg_terminate_backend(pid)
        FROM pg_stat_activity
        WHERE (now() - pg_stat_activity.query_start) > interval '1 minutes' AND usename = 'web';
```
Post your submission to a publicly accessible Github or other SCCS and send us a link.

[+ You can find automation script in automation folder. I used ansible for the upgrade postgresql. This is simple major version upgrade script. in inventory file, must fill the all variables. The script upgrade pg_version variable to pg_upgrade_version variable. Bonus implemented in playbook script. +]

```bash
#In automation folder execute this command
ansible-playbook -i inventory pg_upgrade.yml

```

3. Create a NoSQL DB (MongoDB)

MongoDB is one of the most popular NoSQL Databases out there, as such, is one of the go-to options for multiple scenarios. Given Point 1 (for PostgreSQL) let’s achieve the same on MongoDB:

```
Create a new MongoDB database server
●	Bulk insert/populate multiple documents at once in a collection. Follow the same schema given previously:
    ○	Users
        ■	With administrative privileges
        ■	With write privileges
        ■	With read-only privileges
    ○	Music Albums & Tracks
    ○	Rights-Holder(owners of an album or track) with catalogs of albums and or tracks
    ○	Rights-Holder Organization and Users Authentication
    ➕	BONUS (not required)
        ■	Model Compilation Albums OR
        ■	Add the ability to track changes in ownership of albums and tracks for compilations

●	Let’s work it out
```
```
    ○	Sort said documents in ascending order
```
[+ db.album.find().sort({"name": 1}) +]
```
    ○	Filter the documents using a covered query based on Rightsholders.
```

[+ You can find the answer below  +]

```mongodb
db.artist.aggregate([
    { $match: { name: "Mehmet Emin KARAKAŞ" } },
    {
        $lookup:
        {
            from: "album",
            localField: "_id",
            foreignField: "artist_id",
            as: "albums"
        }
    },
    {
        $unwind: "$albums"
    }
])
```

```
    ○	Calculate the Global Quantity.
```

[+ You can find the answer below  +]

```mongodb
db.artist.aggregate([
    { $match: { name: "Mehmet Emin KARAKAŞ" } },
    {
        $lookup:
        {
            from: "album",
            localField: "_id",
            foreignField: "artist_id",
            as: "albums"
        }
    },
    {
        $unwind: "$albums"
    },
    {
        $group: {
          _id: { name: "$name" },
          count: { $sum: 1 }
        }

    }
])
```

```
    ○	Configure the cache size for MMAPv1
```

[+  In version 4.2, MongoDB removes the deprecated MMAPv1 storage engine. Before version 4.2, All free memory on machine in the MMAPv1 storage engine is used as cache. +]

```
●	Dump it
○	Create a database dump containing the schema and anything else needed for a reviewer to reconstitute your database.
○	Post your submission to a publicly accessible Github or other SCCS and send us a link.
💡	Hint: Don’t assume the reviewer has the same environment as you do
```


[+ You can find the dump(mongo_dump) folder in this repostory. +]

[+ You need to change the security settings in mongod.conf for enable authorization. +]

```
security:
  authorization: enabled
```

[+ user: pex_readonly password: pex_readonly +]

[+ user: pex_superuser password: pex_superuser +]

[+ user: pex_write password: pex_write +]


4. Q & A

In this section you will find some questions. Please provide your answers in as much or little detail as you desire. Respond to your Pex point-of-contact with your answers in whatever format you’d like (pdf, google doc, body of the email) 

```
1.	In your opinion, what conditions indicate whether a relational (tabular) database or NoSQL (Mongo, FDB, etc) is appropriate for modeling some entity(s)?
    ●	Considering #1 above, would a NoSQL database be a better choice than Postgres?
```
[+ If data consistency is important, must choose the rdbms. This example include a lot of relations. This is depend on what you want. May be store data on postgresql. At the same time, denormalize the data and store on nosql database. You can use nosql databases for real time analytics. +]

```
2.	What are your thoughts about triggers in databases?
```

[+ Triggers say the database engine execute this code blocks when an event happen(insert,update,delete)(Before or after). With triggers, you can check data integrity, log anything or execute business logic. But if use the triggers, your dml(insert, update, delete) sql commands are slow down. If use unsecure pl languages, this may cause security issues. +]

```
3.	FoundationDB is a great example of a NoSQL database, however, it often challenges with regards of Old Transactions, not allowing you to read or commit new information. What’s the principle behind that error and how do we go around it?
```

[+ If your read request timestamp is grater then five seconds, you can get this error. The data server kept  version of only last 5 seconds data on the ram. The storage system flushed the data from its in-memory multi-version data structure to its on-disk single-version data structure.  If retry the request with new timestamp, you can acsses the data. The reason may be long transactions, network saturations . +]

```
4.	When should you run a database completely in-memory with no permanent data storage besides a backup?
```
[+ If need more speed , this is useful. Because, all data stored in memory. Acssess data very fast. If your machine is down, lost all your datas.  So, Your data must be regeneretable. If ram is full, you can get oom errors. You can  use for real-time analysis and reporting of data. +]

```
5.	Something fun…. 😀
●	ZFS - A groundbreaking force for good, or an impossibly over-complicated way to store data on disk that will bite you in the worst way?
    ○	What’s your favorite filesystem?
```

[+ I think, new generation file systems like zfs or btrfs are very complicated. It designed for large scale storage. zfs have a lot of advanteges like dedublication, snapshot compression, checksums.  But Still old generation file systems(ext4 or zfs) are more powerfull at database side. EXT4 is the best for now. +]


We expect this assignment to take no longer than 4 hours to complete, and while we are looking to move quickly with hiring for this role, we understand that you may have other job processes and work/life responsibilities taking up your time, so do not hesitate to let us know if you have any questions, require any special accommodations, or need additional time in order to complete this.

